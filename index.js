console.log("hello gitlab_1");
console.log("Hello world_2");
const sup = require("supertest");
const url = "http://109.106.255.69:2208/";
describe("User Login", () => {
  test("Login", async () => {
    const body = await sup(url).post("student/login").send({
      email: "m@gmail.com",
      password: "0910",
    });
    console.log(body.text);
    expect(body.statusCode).toBe(200);
  });
});
describe(" User add contact", () => {
  test("add contact", async () => {
    const body = await sup(url)
      .post("contact/addContact")
      .send({
        name: "ajay",
        phone: 9175285707,
        email: "a@gmail.com",
        description: "coder",
      })
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkZW50SWQiOjEwOCwiaWF0IjoxNjYyMDAxMzQ4LCJleHAiOjE2NjIwMDQ5NDh9.0-43JdEvZX5iAeIp1YU-Cpbz33pPfK793aL8aWm0TlA"
      );
    console.log(body.text);
    expect(body.statusCode).toBe(200);
  });
});
describe("get all user", () => {
  test("get user", async () => {
    const body = await sup(url)
      .get("contact/getAllContactByStudentId/108")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkZW50SWQiOjEwOCwiaWF0IjoxNjYyMDAxMzQ4LCJleHAiOjE2NjIwMDQ5NDh9.0-43JdEvZX5iAeIp1YU-Cpbz33pPfK793aL8aWm0TlA"
      );
    console.log(body.text);
    expect(body.statusCode).toBe(200);
  });
});
describe("update all user", () => {
  test("put user", async () => {
    const body = await sup(url)
      .put("contact/updateContactById/564")
      .send({
        id: 564,
        name: "chirag",
        phone: "886598989",
        email: "c@gmail.com",
        description: "coder\n",
      })
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkZW50SWQiOjEwOCwiaWF0IjoxNjYyMDAxMzQ4LCJleHAiOjE2NjIwMDQ5NDh9.0-43JdEvZX5iAeIp1YU-Cpbz33pPfK793aL8aWm0TlA"
      );
    console.log(body.text);
    expect(body.statusCode).toBe(200);
  });
});
describe("delete request", () => {
  test("delete request", async () => {
    const body = await sup(url)
      .delete("contact/deleteContactById/586")
      .set(
        "x-access-token",
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdHVkZW50SWQiOjExMiwiaWF0IjoxNjYxOTY4ODc4LCJleHAiOjE2NjE5NzI0Nzh9.zVjfjBEFCo_GsGV4lWiWe0yPoaem-YgVp_JDhrhlt-o"
      );
    console.log(body.text);
    expect(body.statusCode).toBe(200);
  });
});
